# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the vvave package.
# SPDX-FileCopyrightText: 2023, 2024, 2025 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: vvave\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-03-07 00:41+0000\n"
"PO-Revision-Date: 2025-01-15 00:08+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Proxecto Trasno (proxecto@trasno.gal)\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.12.0\n"

#: src/main.cpp:77
#, kde-format
msgid "Organize and listen to your music."
msgstr "Organice e escoite música."

#: src/main.cpp:82 src/main.cpp:83
#, kde-format
msgid "Developer"
msgstr "Desenvolvemento."

#: src/main.qml:331 src/main.qml:474 src/widgets/BabeTable/BabeTable.qml:156
#: src/widgets/PlaylistsView/PlaylistsViewModel.qml:60
#: src/widgets/SelectionBar.qml:71
#, kde-format
msgid "Remove"
msgstr "Eliminar"

#: src/main.qml:351
#, kde-format
msgid "Missing file"
msgstr "Ficheiro que falta"

#: src/main.qml:417
#, kde-format
msgid "Stop and Close"
msgstr "Deter e pechar"

#: src/main.qml:418
#, kde-format
msgid "Are you sure you want to stop the music playing and exit?"
msgstr "Seguro que quere deter a música e saír?"

#: src/main.qml:428 src/widgets/SettingsView/SettingsDialog.qml:87
#, kde-format
msgid "Ask before closing if music is playing"
msgstr "Preguntar antes de pechar se hai música reproducíndose."

#: src/main.qml:467 src/widgets/BabeTable/BabeTable.qml:149
#, kde-format
msgid "Remove track"
msgid_plural "Remove %1 tracks"
msgstr[0] "Eliminar a pista"
msgstr[1] "Eliminar as %1 pistas"

#: src/main.qml:468
#, kde-format
msgid ""
"Are you sure you want to remove these files? This action can not be undone."
msgstr ""
"Seguro que quere eliminar estes ficheiros? Esta acción non se pode desfacer."

#: src/main.qml:484 src/widgets/BabeTable/BabeTable.qml:169
#: src/widgets/SleepTimerDialog.qml:154
#, kde-format
msgid "Cancel"
msgstr "Cancelar"

#: src/main.qml:518
#, kde-format
msgid "Saved"
msgstr "Gardouse"

#: src/main.qml:518
#, kde-format
msgid "Track added to playlist"
msgstr "A pista engadiuse á lista de reprodución."

#: src/main.qml:645 src/widgets/SleepTimerDialog.qml:11
#, kde-format
msgid "Sleep Timer"
msgstr "Temporizador para durmir"

#: src/main.qml:654 src/widgets/SettingsView/ShortcutsDialog.qml:12
#, kde-format
msgid "Shortcuts"
msgstr "Atallos"

#: src/main.qml:661
#, kde-format
msgid "Settings"
msgstr "Configuración"

#: src/main.qml:668
#, kde-format
msgid "About"
msgstr "Sobre"

#: src/main.qml:701
#, kde-format
msgid "Songs"
msgstr "Cancións"

#: src/main.qml:721 src/widgets/AlbumsView.qml:76
#, kde-format
msgid "Albums"
msgstr "Álbums"

#: src/main.qml:728
#, kde-format
msgid "No Albums!"
msgstr "Non hai álbums."

#: src/main.qml:729 src/main.qml:753 src/widgets/TracksView.qml:20
#, kde-format
msgid "Add new music sources"
msgstr "Engadir novas fontes de música"

#: src/main.qml:745 src/widgets/AlbumsView.qml:76
#, kde-format
msgid "Artists"
msgstr "Intérpretes"

#: src/main.qml:752
#, kde-format
msgid "No Artists!"
msgstr "Non hai intérpretes."

#: src/main.qml:770 src/widgets/SelectionBar.qml:46
#, kde-format
msgid "Tags"
msgstr "Etiquetas"

#: src/main.qml:779
#, kde-format
msgid "Folders"
msgstr "Cartafoles"

#: src/models/playlists/playlistsmodel.cpp:110
#, kde-format
msgid "Most Played"
msgstr "Máis reproducido"

#: src/models/playlists/playlistsmodel.cpp:117
#, kde-format
msgid "Random Tracks"
msgstr "Pistas ao chou"

#: src/models/playlists/playlistsmodel.cpp:124
#, kde-format
msgid "Recent Tracks"
msgstr "Pistas recentes"

#: src/models/playlists/playlistsmodel.cpp:131
#, kde-format
msgid "Never Played"
msgstr "Non reproducido"

#: src/models/playlists/playlistsmodel.cpp:138
#, kde-format
msgid "Classic Tracks"
msgstr "Pistas clásicas"

#: src/services/local/powermanagementinterface.cpp:173
#: src/services/local/powermanagementinterface.cpp:223
#, kde-format
msgctxt "explanation for sleep inhibit during play of music"
msgid "Playing music"
msgstr "Reproducindo música"

#: src/widgets/AlbumsView.qml:35 src/widgets/FoldersView/FoldersView.qml:51
#: src/widgets/TracksView.qml:25
#, kde-format
msgid "Add sources"
msgstr "Engadir fontes"

#: src/widgets/AlbumsView.qml:70
#, kde-format
msgid "This list is empty"
msgstr "A lista está baleira."

#: src/widgets/BabeGrid/BabeGrid.qml:42 src/widgets/BabeTable/BabeTable.qml:108
#: src/widgets/CloudView/CloudView.qml:24
#: src/widgets/PlaylistsView/PlaylistsViewModel.qml:91
#, kde-format
msgid "Filter"
msgid_plural "Filter %1 albums"
msgstr[0] "Filtrar"
msgstr[1] "Filtrar %1 álbums"

#: src/widgets/BabeTable/BabeTable.qml:74
#, kde-format
msgid "Shuffle Play"
msgstr "Reproducir aleatoriamente"

#: src/widgets/BabeTable/BabeTable.qml:81 src/widgets/TracksGroup.qml:47
#, kde-format
msgid "Play All"
msgstr "Reproducilo todo"

#: src/widgets/BabeTable/BabeTable.qml:88 src/widgets/TracksGroup.qml:55
#, kde-format
msgid "Append All"
msgstr "Engadilo todo"

#: src/widgets/BabeTable/BabeTable.qml:150
#, kde-format
msgid ""
"Are you sure you want to delete the file from your computer? This action can "
"not be undone."
msgstr ""
"Seguro que quere eliminar o ficheiro? Esta acción non se pode desfacer."

#: src/widgets/BabeTable/BabeTable.qml:183 src/widgets/TracksGroup.qml:68
#, kde-format
msgid "Go to Artist"
msgstr "Ir á intérprete"

#: src/widgets/BabeTable/BabeTable.qml:190 src/widgets/TracksGroup.qml:75
#, kde-format
msgid "Go to Album"
msgstr "Ir ao álbum"

#: src/widgets/BabeTable/TableMenu.qml:36
#, kde-format
msgid "Fav it"
msgstr "Facer favorito"

# skip-rule: trasno-remove_reverse
#: src/widgets/BabeTable/TableMenu.qml:36
#, kde-format
msgid "UnFav it"
msgstr "Retirar dos favoritos"

#: src/widgets/BabeTable/TableMenu.qml:46 src/widgets/MetadataDialog.qml:16
#: src/widgets/PlaylistsView/PlaylistsViewModel.qml:52
#, kde-format
msgid "Edit"
msgstr "Editar"

#: src/widgets/BabeTable/TableMenu.qml:56 src/widgets/FocusView.qml:523
#: src/widgets/SelectionBar.qml:54
#, kde-format
msgid "Share"
msgstr "Compartir"

#: src/widgets/BabeTable/TableMenu.qml:66
#, kde-format
msgid "Add to '%1'"
msgstr ""

#: src/widgets/BabeTable/TableMenu.qml:76
#, kde-format
msgid "Add to playlist"
msgstr "Engadir á lista de reprodución"

#: src/widgets/BabeTable/TableMenu.qml:86
#, kde-format
msgid "Select"
msgstr "Seleccionar"

#: src/widgets/BabeTable/TableMenu.qml:99
#, kde-format
msgid "Play Next"
msgstr "Reproducir a seguinte"

#: src/widgets/BabeTable/TableMenu.qml:124
#, kde-format
msgid "Show in Folder"
msgstr "Amosar no cartafol"

#: src/widgets/BabeTable/TableMenu.qml:150
#, kde-format
msgid "Delete"
msgstr "Eliminar"

#: src/widgets/CloudView/CloudView.qml:39
#, kde-format
msgid "Opps!"
msgstr "Vaites!"

#: src/widgets/CloudView/CloudView.qml:40
#, kde-format
msgid ""
"You don't have an account set up.\n"
"You can set up your account now by clicking here or under the Accounts "
"options in the main menu"
msgstr ""
"Non ten unha conta configurada.\n"
"Para configurala agora prema aquí ou baixo as opcións de contas no menú "
"principal."

#: src/widgets/FocusView.qml:158
#, kde-format
msgid "Find"
msgstr "Atopar"

#: src/widgets/FocusView.qml:200
#, kde-format
msgid "No Results!"
msgstr "Non hai resultados."

#: src/widgets/FocusView.qml:201
#, kde-format
msgid "Try with something else"
msgstr "Probar outra cousa"

#: src/widgets/FocusView.qml:240 src/widgets/MainPlaylist/MainPlaylist.qml:153
#, kde-format
msgid "Start putting together your playlist."
msgstr "Prepare a súa lista de reprodución."

#: src/widgets/FocusView.qml:488
#, kde-format
msgid "Favorite"
msgstr "Favorita"

#: src/widgets/FocusView.qml:495
#, kde-format
msgid "Info"
msgstr "Información"

#: src/widgets/FocusView.qml:514
#, kde-format
msgid "Mini Mode"
msgstr "Modo mini"

#: src/widgets/FocusView.qml:532
#, kde-format
msgid "Save"
msgstr "Gardar"

#: src/widgets/FoldersView/FoldersView.qml:33
#, kde-format
msgid "Filter %1 folder"
msgid_plural "Filter %1 folders"
msgstr[0] "Filtrar %1 cartafol"
msgstr[1] "Filtrar %1 cartafoles"

#: src/widgets/FoldersView/FoldersView.qml:47
#, kde-format
msgid "No Folders!"
msgstr "Non hai cartafoles."

#: src/widgets/FoldersView/FoldersView.qml:48
#, kde-format
msgid "Add new music to your sources to browse by folders"
msgstr "Engada música ás fontes para explorar por cartafoles."

#: src/widgets/FoldersView/FoldersView.qml:135 src/widgets/TracksView.qml:19
#, kde-format
msgid "No Tracks!"
msgstr "Non hai pistas."

#: src/widgets/FoldersView/FoldersView.qml:136
#, kde-format
msgid "This source folder seems to be empty!"
msgstr "O cartafol parece estar baleiro."

#: src/widgets/InfoView/InfoView.qml:34
#, kde-format
msgid "Artist Info"
msgstr "Información da intérprete"

#: src/widgets/InfoView/InfoView.qml:61
#, kde-format
msgid "Album Info"
msgstr "Información do álbum"

#: src/widgets/MainPlaylist/MainPlaylist.qml:243
#, kde-format
msgid "Syncing to "
msgstr "Sincronizando con"

#: src/widgets/MetadataDialog.qml:41
#, kde-format
msgid "Metadata"
msgstr "Metadatos"

#: src/widgets/MetadataDialog.qml:42
#, kde-format
msgid "Embedded metadata info."
msgstr "Metadatos incrustados."

#: src/widgets/MetadataDialog.qml:46
#, kde-format
msgid "Track Title"
msgstr "Título da pista"

#: src/widgets/MetadataDialog.qml:58
#, kde-format
msgid "Artist"
msgstr "Intérprete"

#: src/widgets/MetadataDialog.qml:71
#, kde-format
msgid "Album"
msgstr "Álbum"

#: src/widgets/MetadataDialog.qml:84
#, kde-format
msgid "Track"
msgstr "Pista"

#: src/widgets/MetadataDialog.qml:97
#, kde-format
msgid "Genre"
msgstr "Xénero"

#: src/widgets/MetadataDialog.qml:110
#, kde-format
msgid "Year"
msgstr "Ano"

#: src/widgets/MetadataDialog.qml:123
#, kde-format
msgid "Comment"
msgstr "Comentario"

#: src/widgets/PlaybackBar.qml:22
#, kde-format
msgid "Toogle SideBar"
msgstr "Conmutar a barra lateral"

#: src/widgets/PlaybackBar.qml:52
#, kde-format
msgid "Play and pause"
msgstr "Reproducir e pausar"

#: src/widgets/PlaybackBar.qml:62
#, kde-format
msgid "Next"
msgstr "Seguinte"

#: src/widgets/PlaylistsView/PlaylistsView.qml:57
#, kde-format
msgid "Your playlist is empty. Start adding new music to it"
msgstr "A lista de reprodución está baleira. Engádalle música."

#: src/widgets/PlaylistsView/PlaylistsView.qml:68
#, kde-format
msgid "Remove from playlist"
msgstr "Retirar da lista de reprodución"

#: src/widgets/PlaylistsView/PlaylistsViewModel.qml:24
#, kde-format
msgid "No Playlists!"
msgstr "Non hai listas de reprodución."

#: src/widgets/PlaylistsView/PlaylistsViewModel.qml:25
#, kde-format
msgid "Start creating new custom playlists"
msgstr "Comece a crear novas listas de reprodución"

#: src/widgets/PlaylistsView/PlaylistsViewModel.qml:34
#, kde-format
msgid "Remove '%1'?"
msgstr "Retirar «%1»?"

#: src/widgets/PlaylistsView/PlaylistsViewModel.qml:35
#, kde-format
msgid ""
"Are you sure you want to remove this tag? This operation can not be undone."
msgstr ""
"Seguro que quere eliminar a etiqueta? A operación non se pode desfacer."

#: src/widgets/SelectionBar.qml:28
#, kde-format
msgid "Play"
msgstr "Reproducir"

#: src/widgets/SelectionBar.qml:39
#, kde-format
msgid "Append"
msgstr "Engadir"

#: src/widgets/SelectionBar.qml:61
#, kde-format
msgid "Queue"
msgstr "Pór na fila"

#: src/widgets/SettingsView/SettingsDialog.qml:38
#, kde-format
msgid "Remove source"
msgstr "Retirar a fonte"

#: src/widgets/SettingsView/SettingsDialog.qml:39
#, kde-format
msgid ""
"Are you sure you want to remove the source: \n"
"%1"
msgstr ""
"Seguro que quere eliminar a fonte\n"
"%1?"

#: src/widgets/SettingsView/SettingsDialog.qml:55
#, kde-format
msgid "Playback"
msgstr "Reprodución"

#: src/widgets/SettingsView/SettingsDialog.qml:60
#, kde-format
msgid "Auto Resume"
msgstr "Continuación automática"

#: src/widgets/SettingsView/SettingsDialog.qml:61
#, kde-format
msgid "Resume the last session playlist."
msgstr "Retomar a lista de reprodución da última sesión."

#: src/widgets/SettingsView/SettingsDialog.qml:73
#, kde-format
msgid "Volume"
msgstr "Volume"

#: src/widgets/SettingsView/SettingsDialog.qml:74
#, kde-format
msgid "Show volume controls."
msgstr "Amosar o control de volume."

#: src/widgets/SettingsView/SettingsDialog.qml:86
#, kde-format
msgid "Exiting"
msgstr "Saíndo"

#: src/widgets/SettingsView/SettingsDialog.qml:100
#, kde-format
msgid "Collection"
msgstr "Colección"

#: src/widgets/SettingsView/SettingsDialog.qml:105
#, kde-format
msgid "Fetch Artwork"
msgstr "Obter os gráficos"

#: src/widgets/SettingsView/SettingsDialog.qml:106
#, kde-format
msgid ""
"Gathers album and artists artworks from online services: LastFM, Spotify, "
"MusicBrainz, iTunes, Genius, and others."
msgstr ""
"Recolle os gráficos dos álbums e intérpretes de servizos de Internet: "
"LastFM, Spotify, MusicBrainz, iTunes, Genius e outros."

#: src/widgets/SettingsView/SettingsDialog.qml:118
#, kde-format
msgid "Auto Scan"
msgstr "Examinar automaticamente"

#: src/widgets/SettingsView/SettingsDialog.qml:119
#, kde-format
msgid ""
"Scan all the music sources on startup to keep your collection up to date."
msgstr "Examinar as fontes de música no inicio para manter a colección ao día."

#: src/widgets/SettingsView/SettingsDialog.qml:132
#, kde-format
msgid "General"
msgstr "Xeral"

#: src/widgets/SettingsView/SettingsDialog.qml:137
#, kde-format
msgid "Focus View"
msgstr "Vista de concentración"

#: src/widgets/SettingsView/SettingsDialog.qml:138
#, kde-format
msgid "Make the focus view the default."
msgstr "Facer a vista de concentración a predeterminada."

#: src/widgets/SettingsView/SettingsDialog.qml:153
#, kde-format
msgid "Artwork"
msgstr "Gráficos"

#: src/widgets/SettingsView/SettingsDialog.qml:154
#, kde-format
msgid "Show the cover artwork for the tracks."
msgstr "Amosar os gráficos de portada das pistas."

#: src/widgets/SettingsView/SettingsDialog.qml:169
#, kde-format
msgid "Titles"
msgstr "Títulos"

#: src/widgets/SettingsView/SettingsDialog.qml:170
#, kde-format
msgid "Show the title of albums and artists in the grid view."
msgstr "Amosar o título dos álbums e intérpretes na vista de grade."

#: src/widgets/SettingsView/SettingsDialog.qml:186
#, kde-format
msgid "Sources"
msgstr "Fontes"

#: src/widgets/SettingsView/SettingsDialog.qml:223
#, kde-format
msgid "Add"
msgstr "Engadir"

#: src/widgets/SettingsView/SettingsDialog.qml:241
#, kde-format
msgid "Scan now"
msgstr "Examinar"

#: src/widgets/SettingsView/ShortcutsDialog.qml:26
#: src/widgets/SettingsView/ShortcutsDialog.qml:39
#, kde-format
msgid "Unknown"
msgstr "Descoñecida"

#: src/widgets/SleepTimerDialog.qml:32
#, kde-format
msgid "15 minutes"
msgstr "15 minutos"

#: src/widgets/SleepTimerDialog.qml:49
#, kde-format
msgid "30 minutes"
msgstr "30 minutos"

#: src/widgets/SleepTimerDialog.qml:67
#, kde-format
msgid "1 hour"
msgstr "1 hora"

#: src/widgets/SleepTimerDialog.qml:85
#, kde-format
msgid "End of track"
msgstr "Fin da pista"

#: src/widgets/SleepTimerDialog.qml:103
#, kde-format
msgid "End of playlist"
msgstr "Fin da lista de reprodución"

#: src/widgets/SleepTimerDialog.qml:123
#, kde-format
msgid "Off"
msgstr "Desactivada"

#: src/widgets/SleepTimerDialog.qml:145
#, kde-format
msgid "Close application after"
msgstr "Pechar a aplicación tras"

#: src/widgets/SleepTimerDialog.qml:160
#, kde-format
msgid "Set"
msgstr "Definir"

#: src/widgets/TracksView.qml:31
#, kde-format
msgid "Open file"
msgstr "Abrir un ficheiro"

#~ msgid "Accept"
#~ msgstr "Aceptar"

#~ msgid "Cloud"
#~ msgstr "Nube"

#~ msgid "Dark Mode"
#~ msgstr "Modo escuro"

#~ msgid "Switch between light and dark colorscheme."
#~ msgstr "Cambiar entre os esquemas de cores claro e escuro."

#~ msgid "© 2019-2023 Maui Development Team"
#~ msgstr "© 2019-2023 Equipo de desenvolvemento de Maui"

#~ msgid "Vvave"
#~ msgstr "Vvave"

#~ msgid "Remove %1 tracks"
#~ msgstr "Eliminar as %1 pistas"

#~ msgid "Configure the playback behavior."
#~ msgstr "Configure o comportamento da reprodución."

#~ msgid "Configure the app plugins and collection behavior."
#~ msgstr ""
#~ "Configurar os complementos da aplicación e o comportamento da colección."

#~ msgid "Add or remove sources"
#~ msgstr "Engadir ou retirar fontes"
